<?php

namespace App\Http\Middleware;

use Closure;
use JWTAuth;
use Exception;
use Tymon\JWTAuth\Exceptions\JWTException;
use Tymon\JWTAuth\Exceptions\TokenExpiredException;
use Tymon\JWTAuth\Http\Middleware\BaseMiddleware;

class JwtMiddleware extends BaseMiddleware
{

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        try {
            $user = JWTAuth::parseToken()->authenticate();
            if ((int)$user->banned === 1) {
                return response()
                    ->json([
                        'status' => 'fail',
                        'error' => 'You have been banned by admin'
                    ]);
            }
        } catch (Exception $e) {
            if ($e instanceof \Tymon\JWTAuth\Exceptions\TokenInvalidException) {
                return response()
                    ->json([
                        'status' => 'fail',
                        'error' => 'Token is Invalid'
                    ], 401);
            } else if ($e instanceof \Tymon\JWTAuth\Exceptions\TokenExpiredException) {
                return response()
                    ->json([
                        'status' => 'fail',
                        'error' => 'Token is Expired'
                    ], 401);
            } else {
                return response()
                    ->json([
                        'status' => 'fail',
                        'error' => 'Authorization Token not found'
                    ], 401);
            }
        }
        return $next($request);
    }
}