<?php

namespace App\Http\Controllers;

use App\Transaction;
use App\User;
use Illuminate\Support\Facades\Auth as DefaultAuth;
use Dingo\Api\Auth\Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;

class UserController extends Controller
{
    public function authenticate(Request $request)
    {
        $credentials = $request->only('email', 'password');

        try {
            $token = JWTAuth::attempt($credentials);

            if (!$token) {
                return response()
                    ->json([
                        'status' => 'fail',
                        'error' => 'invalid_credentials'
                    ], 400);
            }

            $user = JWTAuth::user();

            if ((int)$user->banned === 1) {
                return response()
                    ->json([
                        'status' => 'fail',
                        'error' =>  'You have been banned by admin'
                    ], 400);
            }

        } catch (JWTException $e) {
            return response()
                ->json(['status' => 'fail',
                    'error' =>  'could_not_create_token'
                ], 500);
        }

        return response()->json(compact('token'));
    }

    public function register(Request $request)
    {
        $validator = Validator::make($request->json()->all(), [
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6|confirmed',
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors()->toJson(), 400);
        }

        $user = User::create([
            'name' => $request->get('name'),
            'email' => $request->get('email'),
            'password' => Hash::make($request->get('password')),
        ]);



        $token = JWTAuth::fromUser($user);

        return response()->json(compact('user','token'), 201);
    }

    public function test() {
        return 1;
    }

    public function getAuthenticatedUser()
    {
        try {

            if (! $user = JWTAuth::parseToken()->authenticate()) {
                return response()->json(['user_not_found'], 404);
            }

        } catch (Tymon\JWTAuth\Exceptions\TokenExpiredException $e) {

            return response()->json(['token_expired'], $e->getStatusCode());

        } catch (Tymon\JWTAuth\Exceptions\TokenInvalidException $e) {

            return response()->json(['token_invalid'], $e->getStatusCode());

        } catch (Tymon\JWTAuth\Exceptions\JWTException $e) {

            return response()->json(['token_absent'], $e->getStatusCode());

        }

        return response()->json(compact('user'));
    }

    public function showAllWithoutMe(Request $request) {
        $user = JWTAuth::parseToken()->authenticate();
        $users = User::query()
            ->where('id', '!=', $user->id)
            ->where('banned', '!=', 1)
            ->get();

        return response()->json($users);
    }

    public function showMyTransactionsHistory(Request $request) {

        $user = JWTAuth::parseToken()->authenticate();
        $transactions = Transaction::query()
            ->select([
                'transactions.id',
                'transactions.from_id',
                'transactions.to_id',
                'from_data.name as from_name',
                'to_data.name as to_name',
                'amount',
                'transactions.created_at',
                'from_balance',
                'to_balance'
            ])
            ->where(function ($query) use ($user) {
                $query->where('from_id', '=', $user->id)
                    ->orWhere('to_id', '=', $user->id);
            })
            ->join('users as from_data', 'from_id', '=', 'from_data.id')
            ->join('users as to_data', 'to_id', '=', 'to_data.id')
            ->orderBy('transactions.id', 'desc')
            ->get();

        $convertedTransactions = $transactions->toArray();

        foreach ($convertedTransactions as $key => $transaction) {


            if ($transaction['from_id'] == $user->id) {
                $transaction['balance'] = $transaction['from_balance'];
                $transaction['operation'] = '-';
            } elseif ($transaction['to_id'] == $user->id) {
                $transaction['balance'] = $transaction['to_balance'];
                $transaction['operation'] = '+';
            }

            unset($transaction['to_balance']);
            unset($transaction['from_balance']);
            $convertedTransactions[$key] = $transaction;

        }


        return response()->json($convertedTransactions, 200);
    }
}