<?php

namespace App\Http\Controllers;

use App\Transaction;
use http\Env\Response;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use JWTAuth;
use Symfony\Component\HttpFoundation\ParameterBag;
use Tymon\JWTAuth\Exceptions\JWTException;
use Illuminate\Validation\Rule;
use App\User;

class TransactionController extends Controller
{


    private function checkTransaction(Request $request) {
        $user = JWTAuth::parseToken()->authenticate();


        $validator = Validator::make($request->json()->all(), [
            'amount' => 'required|numeric',
            'receiver_id' => 'required|exists:users,id'
        ]);


        $validator->after(function ($validator) use ($user, $request) {

            if ((int)$request->json('receiver_id') === (int)$user->id) {
                $validator->errors()->add('receiver_id', 'You can\'t send money to yourself');
            }

            if ((double)$user->balance < (double)$request->json('amount')) {
                $validator->errors()->add('amount', 'Insufficient funds on balance');
            }

            $countOfBanned = User::query()
                ->where('id', '=', (int)$request->json('receiver_id'))
                ->where('banned', '=', 1)
                ->count();

            if ($countOfBanned > 0) {
                $validator->errors()->add('receiver_id', 'This user banned by admin');
            }
        });


        if ($validator->fails()) {
            return [
                'response_code' => 400,
                'response_body' => [
                    'status' => 'fail',
                    'errors' => $validator->errors()->toArray()
                ]
            ];
        }

        return [
            'response_code' => 200,
            'response_body' => ['status' => 'Now you can commit transaction']
        ];

    }

    public function checkTransactionAmount(Request $request) {
        $checkResult = $this->checkTransaction($request);

        return response()->json($checkResult['response_body'], $checkResult['response_code']);
    }

    public function commitTransaction(Request $request) {
        $checkResult = $this->checkTransaction($request);

        if ($checkResult['response_code'] != 200) {
            return response()->json($checkResult['response_body'], $checkResult['response_code']);
        }

        $user = JWTAuth::parseToken()->authenticate();

        $fromUser = clone $user;
        $toUser = User::query()
            ->where('id', '=', (int)$request->json('receiver_id'))
            ->first();

        $fromUser->balance = (double)$fromUser->balance - (double)$request->json('amount');
        $toUser->balance = (double)$toUser->balance + (double)$request->json('amount');

        $fromUser->save();
        $toUser->save();


        $transaction = new Transaction([
            'from_id' => $user->id,
            'to_id' => (int)$request->json('receiver_id'),
            'amount' => (double)$request->json('amount'),
            'from_balance' => (double)$fromUser->balance,
            'to_balance' => (double)$toUser->balance,
        ]);

        $transaction->save();

        $balance = (double)$fromUser->balance;

        return response()->json(['status' => 'success', 'balance' => $balance], 200);

    }

    public function duplicateTransaction(Request $request) {
        $validator = Validator::make($request->json()->all(), [
            'id' => 'required|integer|exists:transactions,id',
        ]);


        if ($validator->fails()) {
            return [
                'response_code' => 400,
                'response_body' => [
                    'status' => 'fail',
                    'errors' => $validator->errors()->toArray()
                ]
            ];
        }

        $transaction = Transaction::query()
            ->where('id', '=', $request->json('id'))
            ->first();

        if (is_object($transaction) && $transaction->id) {
            $commitRequest = new Request();

            $parameters = new ParameterBag();
            $parameters->set('receiver_id', $transaction->to_id);
            $parameters->set('amount', $transaction->amount);

            $commitRequest->setJson($parameters);
            return $this->commitTransaction($commitRequest);
        }
    }
}
