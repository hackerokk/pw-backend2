<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Transaction extends Model
{

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'from_id', 'to_id', 'amount', 'from_balance', 'to_balance'
    ];

    public function user()
    {
        return $this->belongsTo('App\User', 'from_id', 'id');
    }
}
