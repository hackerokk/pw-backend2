<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

$api = app('Dingo\Api\Routing\Router');
$api->version('v1', ['middleware' => 'cors'], function ($api) {
    $api->post('users/register', '\App\Http\Controllers\UserController@register');
    $api->post('users/login', '\App\Http\Controllers\UserController@authenticate');

    $api->group(['middleware' => 'jwt.verify', 'prefix' => 'users'], function ($api) {
        $api->get('me', '\App\Http\Controllers\UserController@getAuthenticatedUser');

        $api->get('', '\App\Http\Controllers\UserController@showAllWithoutMe');
        $api->get('transactions', '\App\Http\Controllers\UserController@showMyTransactionsHistory');
    });

    $api->group(['middleware' => 'jwt.verify', 'prefix' => 'transactions'], function ($api) {
        $api->post('check', '\App\Http\Controllers\TransactionController@checkTransactionAmount');
        $api->post('commit', '\App\Http\Controllers\TransactionController@commitTransaction');
        $api->post('duplicate', '\App\Http\Controllers\TransactionController@duplicateTransaction');
    });
});